FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
#WORKDIR /app

# libgdiplus 安装
#RUN apt-get update && \
#    apt-get install -y libgdiplus && \
#    cd /usr/lib && \
#    ln -s libgdiplus.so gdiplus.dll


EXPOSE 80
EXPOSE 443

WORKDIR /app
COPY /publish/.  /app

# 装载appsettings.json、web.config文件
COPY .  /app

ENTRYPOINT ["dotnet", "web.dll"]