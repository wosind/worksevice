﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public class RouteDefend
  {
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;
    public RouteDefend(RequestDelegate next, ILoggerFactory loggerFactory)
    {
      _next = next;
      _logger = loggerFactory.CreateLogger<RouteDefend>();
    }

    public async Task Invoke(HttpContext context)
    {
      var _orgin = context.Request.Path;
      _logger.LogInformation("================================");
      _logger.LogInformation("尝试请求："+context.Request.Path);
      _logger.LogInformation("================================");

      if (_orgin == "/api/Open/Auth/GetPhoneNum")
      {
        _logger.LogInformation("请求放行");
        await _next.Invoke(context);
      }
      else {
        _logger.LogInformation("阻止请求");
      }


    }
  }
}

