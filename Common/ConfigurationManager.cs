﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.UserSecrets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common
{
    public static class ConfigurationManager
    {
        public static IConfiguration Configuration; 
         
        /// <summary>
        /// 添加开发时相关配置
        /// </summary>
        /// <param name="builder"></param>
        public static void AddEnvJsonConfig(this IConfigurationBuilder builder)
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configFile = Environment.GetEnvironmentVariable("ConfigFile");
            if (!string.IsNullOrEmpty(configFile))
            {
                builder.AddJsonFile($"appsettings.{configFile}.json", optional: false, reloadOnChange: true);
            }
            else
            {
                if (!string.IsNullOrEmpty(env))
                {
                    builder.AddJsonFile($"appsettings.{env}.json", optional: false, reloadOnChange: true);
                }
                else
                {
                    builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                }
            }
        }

        public static T GetSection<T>(string key)
        {
            var settingsSection = Configuration.GetSection(key);
            var v = settingsSection.Get<T>();
            return v;
        }
    }
}
