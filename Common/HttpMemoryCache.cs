﻿using System;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace Common
{
   public static class HttpMemoryCache
   {
        private static readonly MemoryCache Cache = new MemoryCache(new MemoryCacheOptions());

       public static void Init(IServiceCollection services)
       {
            services.AddDistributedMemoryCache();
        }

       public static object GetCacheValue(string key)
       {
            if (key != null && Cache.TryGetValue(key, out var val))
            {
                return val;
            }
            return null;
        }

       public static void SetChacheValue(string key, object value)
       {
            if (key == null) return;

            Cache.Set(key, value, new MemoryCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromHours(1)
            });
        }

       public static void RemoveChacheValue(string key)
       {
            Cache.Remove(key);
        }
    }
}
