﻿using System;
using System.Runtime.Serialization;

namespace Common
{
  public class IException : Exception
  {
    public IException()
    {

    }

    public IException(string message) : base(message)
    {

    }

    public IException(string message, Exception innerException) : base(message, innerException)
    {
    }

    protected IException(SerializationInfo info, StreamingContext context) : base(info, context)
    {

    }
  }
}
