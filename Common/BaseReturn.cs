﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  /// <summary>
  /// 带状态的返回值
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class BaseReturn <T>
  {
    public Boolean Success { get; set; } = true;
    public string ErrMsg { get; set; }
    public T Data { get; set; }      
  }
}
