﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Common
{
    public static class DictExtension
    {

        public static HtmlString GetValue(this Dictionary<string, object> dict, string key, string defaultValue = "")
        {
            if (!dict.ContainsKey(key) || dict[key] == null) return new HtmlString(defaultValue);
            var value = WebUtility.HtmlDecode(dict[key].ToString());
            return new HtmlString(value);
        }

        public static string GetStringValue(this Dictionary<string, object> dict, string key, string defaultValue = "")
        {
            if (!dict.ContainsKey(key) || dict[key] == null) return defaultValue;
            var value = WebUtility.HtmlDecode(dict[key].ToString());
            return new string(value);
        }

    }
}
