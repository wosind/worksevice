﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Common
{
  /*
   * 
   *  
   *  var settings = new JsonSerializerSettings
      {
        Formatting = Formatting.Indented,
        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        ContractResolver = new JsonPropertyContractResolver(new List<string> { "ApplyJobs" })
      };

      var t = JsonConvert.SerializeObject(res, settings);
   * 
   * */
  public class JsonPropertyContractResolver : DefaultContractResolver
  {
    IEnumerable<string> lstInclude;
    public JsonPropertyContractResolver(IEnumerable<string> includeProperties)
    {
      lstInclude = includeProperties;
    }

    protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
    {
      return base.CreateProperties(type, memberSerialization)
        .ToList()
        .FindAll(p => !lstInclude.Contains(p.PropertyName));
    }
  }
}
