﻿using Hangfire;
using Hangfire.Storage;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Common
{
    public class JobHelper
    {
        public static void AddJob(Expression<Action> methodCall)
        {
            string jobId = BackgroundJob.Enqueue(methodCall);
        }
    }
}
