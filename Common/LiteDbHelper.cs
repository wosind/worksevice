﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class LiteDbHelper
    {
        public static string defaultDb = "Config.db";

        private static string GetDbPath(string dbName = "")
        {
            if (string.IsNullOrEmpty(dbName))
            {
                dbName = defaultDb;
            }
            else
            {
                dbName = "App_Data\\" + dbName;
            }
            return dbName;
        }

        private static ConnectionString GetConnectionString(string dbName = "")
        {
            var dbPath = GetDbPath(dbName);
            ConnectionString connectionString = new ConnectionString();
            connectionString.Filename = dbPath;
            //connectionString.Password = "";
            connectionString.Upgrade = true;
            connectionString.Connection = ConnectionType.Shared;
            return connectionString;
        }

        private static LiteDatabase GetDb(string dbName = "")
        {
            var connStr = GetConnectionString(dbName);
            return new LiteDatabase(connStr);
        }

        private static LiteRepository GetRepository(string dbName = "")
        {
            var connStr = GetConnectionString(dbName);
            return new LiteRepository(connStr);
        }

        /// <summary>
        /// 执行LiteDb数据库操作，需要先创建集合对象
        /// 示例: var col = db.GetCollection<LoginConfirm>("LoginConfirm");
        /// </summary>
        /// <param name="func"></param>
        /// <param name="dbName"></param>
        public static void Execute(Action<LiteDatabase> func, string dbName = "")
        {
            using (var db = GetDb(dbName))
            {
                func(db);
            }
        }

        /// <summary>
        /// 执行LiteDb数据库操作，需要先创建集合对象
        /// 示例: var col = db.GetCollection<LoginConfirm>("LoginConfirm");
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static T Execute<T>(Func<LiteDatabase, T> func, string dbName = "")
        {
            using (var db = GetDb(dbName))
            {
                return func(db);
            }
        }

        /// <summary>
        /// 执行LiteDb数据库操作
        /// 简洁方式，不用获取集合
        /// </summary>
        /// <param name="func"></param>
        /// <param name="dbName"></param>
        public static void Exec(Action<LiteRepository> func, string dbName = "")
        {
            using (var db = GetRepository(dbName))
            {
                func(db);
            }
        }

        /// <summary>
        /// 执行LiteDb数据库操作
        /// 简洁方式，不用获取集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static T Exec<T>(Func<LiteRepository, T> func, string dbName = "")
        {
            using (var db = GetRepository(dbName))
            {
                return func(db);
            }
        }


        public static void InsertBulk<T>(string tableName, List<T> list, bool isClearAll = false)
        {
            Execute((db) =>
            {
                var col = db.GetCollection<T>(tableName);
                if (isClearAll)
                {
                    col.DeleteAll();
                }
                col.InsertBulk(list);
            });
        }
    }
}
