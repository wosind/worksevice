﻿
using Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DataRepository
{
  public interface IWorkRepository:IAutoAddDenpendency
  {
    DataPageResult<Work> GetWorkList(int page,int limit);
    Work AddItem(Work work);
    bool IsExists(Work work);
    Work GetDetailById(string id);
  }
}
