﻿
using Models.DataBase;
using System;
using System.Linq.Expressions;

namespace DataRepository
{
  public interface IAccountRepository : IAutoAddDenpendency
  {
    Account GetAccountByWid(string Wid);
    Account AddItem(Account _account);

  }
}
