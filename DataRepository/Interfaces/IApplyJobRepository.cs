﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.DataBase;

namespace DataRepository
{
  public interface IApplyJobRepository : IAutoAddDenpendency
  {
    ApplyJob AddItem(ApplyJob applyJob);
    bool IsExists(ApplyJob applyJob);
    List<ApplyJob> GetApplyJobs(string accountId);
  }
}
