﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DataRepository
{
  public interface IRepository<TEntity>
  {
    TEntity Insert(TEntity T);
    TEntity Update(TEntity T);
    TEntity Delete(TEntity T);

    TEntity GetDetailById(Expression<Func<TEntity, bool>> exp);

    List<TEntity> GetDataList();
    List<TEntity> GetDataList(Expression<Func<TEntity,bool>> exp);
    List<TEntity> GetDataPage(int page,int limit,out int count);
    List<TEntity> GetDataPage(Expression<Func<TEntity,bool>> exp,int page,int limit,out int count);
  }
}
