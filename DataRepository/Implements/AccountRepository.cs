﻿using Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository
{

  class AccountRepository : Repository<Account>, IAccountRepository
  {
    public AccountRepository(WorkContext dbContext) : base(dbContext)
    {
    }

    public Account AddItem(Account _account)
    {
      if (_account == null) return null;
      return base.Insert(_account);
    }

    public Account GetAccountByWid(string wid)
    {
      if (wid == null) return null;
      return base._dbContext.Set<Account>().FirstOrDefault(c => c.Wid == wid);
    }
  }
}
