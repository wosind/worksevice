﻿using Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository
{
  class WorkRepository : Repository<Work>, IWorkRepository
  {
    public WorkRepository(WorkContext dbContext) : base(dbContext)
    {
    }

    public Work AddItem(Work work)
    {
      if (work == null) return null;
      return base.Insert(work);
    }

    public Work GetDetailById(string id)
    {
      if (id == null) return null;
      return base.GetDetailById(c => c.Gid == id);
    }

    public DataPageResult<Work> GetWorkList(int page, int limit)
    {
      var dlist = base.GetDataPage(page, limit, out int count);
      var res = new DataPageResult<Work>
      {
        Count = count,
        List = dlist
      };
      return res;
    }

    public bool IsExists(Work work)
    {
      var res = _dbContext.Set<Work>().FirstOrDefault(c => c.Gid == work.Gid);
      return res != null;
    }
  }
}
