﻿using Microsoft.EntityFrameworkCore;
using Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
namespace DataRepository
{
  class ApplyJobRepository : Repository<ApplyJob>, IApplyJobRepository
  {
    public ApplyJobRepository(WorkContext dbContext) : base(dbContext)
    {
    }

    public ApplyJob AddItem(ApplyJob applyJob)
    {
      if (applyJob == null) return null;
      return base.Insert(applyJob);
    }

    public List<ApplyJob> GetApplyJobs(string accountId)
    {
      var res = _dbContext.Set<ApplyJob>()
        .Where(c=>c.AccountId == accountId)
        .Include(p => p.Work)
        .Include(p => p.Account)       
        .ToList();
      return res;    
    }

    public bool IsExists(ApplyJob applyJob)
    {
      var res = base.GetDetailById(c => c.AccountId == applyJob.AccountId && c.WorkId == applyJob.WorkId);
      return res != null;
    }

   
  }
}
