﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Models.DataBase
{
    public partial class WorkContext : DbContext
    {
        public WorkContext()
        {
        }

        public WorkContext(DbContextOptions<WorkContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<ApplyJob> ApplyJobs { get; set; }
        public virtual DbSet<Work> Works { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=175.178.25.219;User Id=root;Password=@Xkj753159;Database=xxdata;", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.6.50-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8")
                .UseCollation("utf8_general_ci");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.Gid)
                    .HasName("PRIMARY");

                entity.ToTable("account");

                entity.Property(e => e.Gid)
                    .HasMaxLength(100)
                    .HasColumnName("gid");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createTime");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .HasColumnName("phone");

                entity.Property(e => e.Roles)
                    .HasMaxLength(255)
                    .HasColumnName("roles");

                entity.Property(e => e.Token)
                    .HasMaxLength(255)
                    .HasColumnName("token");

                entity.Property(e => e.Wid)
                    .HasMaxLength(100)
                    .HasColumnName("wid");
            });

            modelBuilder.Entity<ApplyJob>(entity =>
            {
                entity.HasKey(e => e.Gid)
                    .HasName("PRIMARY");

                entity.ToTable("applyJob");

                entity.HasIndex(e => e.AccountId, "fk_accountid");

                entity.HasIndex(e => e.WorkId, "fk_workid");

                entity.Property(e => e.Gid)
                    .HasMaxLength(100)
                    .HasColumnName("gid");

                entity.Property(e => e.AccountId)
                    .HasMaxLength(100)
                    .HasColumnName("accountId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createTime");

                entity.Property(e => e.EmployId)
                    .HasMaxLength(100)
                    .HasColumnName("employId");

                entity.Property(e => e.LastReply)
                    .HasColumnType("datetime")
                    .HasColumnName("lastReply");

                entity.Property(e => e.State)
                    .HasMaxLength(20)
                    .HasColumnName("state");

                entity.Property(e => e.StateText)
                    .HasMaxLength(100)
                    .HasColumnName("stateText");

                entity.Property(e => e.WorkId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("workId");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.ApplyJobs)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("fk_accountid");

                entity.HasOne(d => d.Work)
                    .WithMany(p => p.ApplyJobs)
                    .HasForeignKey(d => d.WorkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_workid");
            });

            modelBuilder.Entity<Work>(entity =>
            {
                entity.HasKey(e => e.Gid)
                    .HasName("PRIMARY");

                entity.ToTable("works");

                entity.Property(e => e.Gid)
                    .HasMaxLength(100)
                    .HasColumnName("gid");

                entity.Property(e => e.Addr)
                    .HasMaxLength(255)
                    .HasColumnName("addr")
                    .HasComment("用人单位地址");

                entity.Property(e => e.Award)
                    .HasPrecision(16, 2)
                    .HasColumnName("award")
                    .HasComment("福利");

                entity.Property(e => e.AwardTips)
                    .HasMaxLength(255)
                    .HasColumnName("awardTips")
                    .HasComment("福利说明");

                entity.Property(e => e.CountOfNeed)
                    .HasMaxLength(255)
                    .HasColumnName("countOfNeed")
                    .HasComment("招聘人数");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createTime")
                    .HasComment("创建时间");

                entity.Property(e => e.EmployId)
                    .HasMaxLength(100)
                    .HasColumnName("employId")
                    .HasComment("用人单位编号");

                entity.Property(e => e.EmployName)
                    .HasMaxLength(255)
                    .HasColumnName("employName")
                    .HasComment("用人单位名称");

                entity.Property(e => e.Location)
                    .HasMaxLength(255)
                    .HasColumnName("location")
                    .HasComment("用人单位位置");

                entity.Property(e => e.MaxAge)
                    .HasColumnType("int(11)")
                    .HasColumnName("maxAge")
                    .HasComment("最大年龄");

                entity.Property(e => e.MaxPay)
                    .HasColumnType("int(11)")
                    .HasColumnName("maxPay")
                    .HasComment("最多工资");

                entity.Property(e => e.MinAge)
                    .HasColumnType("int(11)")
                    .HasColumnName("minAge")
                    .HasComment("最小年龄");

                entity.Property(e => e.MinPay)
                    .HasColumnType("int(11)")
                    .HasColumnName("minPay")
                    .HasComment("最少工资");

                entity.Property(e => e.Notes)
                    .HasColumnType("text")
                    .HasColumnName("notes")
                    .HasComment("备注");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .HasColumnName("phone")
                    .HasComment("联系人");

                entity.Property(e => e.SocialSecurity)
                    .HasMaxLength(255)
                    .HasColumnName("socialSecurity")
                    .HasComment("社保");

                entity.Property(e => e.WorkName)
                    .HasMaxLength(100)
                    .HasColumnName("workName")
                    .HasComment("职位名称");

                entity.Property(e => e.WorkType)
                    .HasMaxLength(40)
                    .HasColumnName("workType")
                    .HasComment("职位类型");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
