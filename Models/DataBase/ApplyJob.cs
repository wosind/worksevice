﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.DataBase
{
    public partial class ApplyJob
    {
        public string Gid { get; set; }
        public DateTime? CreateTime { get; set; }
        public string WorkId { get; set; }
        public string State { get; set; }
        public string StateText { get; set; }
        public DateTime? LastReply { get; set; }
        public string AccountId { get; set; }
        public string EmployId { get; set; }

        public virtual Account Account { get; set; }
        public virtual Work Work { get; set; }
    }
}
