﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Models.DataBase
{
    public partial class Account
    {
        public Account()
        {
            ApplyJobs = new HashSet<ApplyJob>();
        }

        public string Gid { get; set; }
        public string Name { get; set; }
        public string Roles { get; set; }
        public string Wid { get; set; }
        public string Phone { get; set; }
        public DateTime? CreateTime { get; set; }
        public string Token { get; set; }

        public virtual ICollection<ApplyJob> ApplyJobs { get; set; }
    }
}
