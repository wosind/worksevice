﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
#nullable disable

namespace Models.DataBase
{
  public partial class Work
  {
    public Work()
    {
      ApplyJobs = new HashSet<ApplyJob>();
    }

    public string Gid { get; set; }
    public DateTime? CreateTime { get; set; }
    public string Location { get; set; }
    public string WorkType { get; set; }
    public string CountOfNeed { get; set; }
    public int? MinAge { get; set; }
    public int? MaxAge { get; set; }
    public int? MinPay { get; set; }
    public int? MaxPay { get; set; }
    public string Notes { get; set; }
    public string WorkName { get; set; }
    public string Addr { get; set; }
    public string EmployName { get; set; }
    public string AwardTips { get; set; }
    public decimal? Award { get; set; }
    public string Phone { get; set; }
    public string SocialSecurity { get; set; }
    public string EmployId { get; set; }   
    public virtual ICollection<ApplyJob> ApplyJobs { get; set; }
  }
}
