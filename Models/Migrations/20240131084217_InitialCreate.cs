﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8");

            migrationBuilder.CreateTable(
                name: "account",
                columns: table => new
                {
                    gid = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    roles = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    wid = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    phone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    createTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    token = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.gid);
                })
                .Annotation("MySql:CharSet", "utf8")
                .Annotation("Relational:Collation", "utf8_general_ci");

            migrationBuilder.CreateTable(
                name: "works",
                columns: table => new
                {
                    gid = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    createTime = table.Column<DateTime>(type: "datetime", nullable: true, comment: "创建时间"),
                    location = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, comment: "用人单位位置", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    workType = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: true, comment: "职位类型", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    countOfNeed = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, comment: "招聘人数", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    minAge = table.Column<int>(type: "int(11)", nullable: true, comment: "最小年龄"),
                    maxAge = table.Column<int>(type: "int(11)", nullable: true, comment: "最大年龄"),
                    minPay = table.Column<int>(type: "int(11)", nullable: true, comment: "最少工资"),
                    maxPay = table.Column<int>(type: "int(11)", nullable: true, comment: "最多工资"),
                    notes = table.Column<string>(type: "text", nullable: true, comment: "备注", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    workName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, comment: "职位名称", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    addr = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, comment: "用人单位地址", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    employName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, comment: "用人单位名称", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    awardTips = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, comment: "福利说明", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    award = table.Column<decimal>(type: "decimal(16,2)", precision: 16, scale: 2, nullable: true, comment: "福利"),
                    phone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true, comment: "联系人", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    socialSecurity = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, comment: "社保", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    employId = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, comment: "用人单位编号", collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.gid);
                })
                .Annotation("MySql:CharSet", "utf8")
                .Annotation("Relational:Collation", "utf8_general_ci");

            migrationBuilder.CreateTable(
                name: "applyJob",
                columns: table => new
                {
                    gid = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    createTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    workId = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    state = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    stateText = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    lastReply = table.Column<DateTime>(type: "datetime", nullable: true),
                    accountId = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8"),
                    employId = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true, collation: "utf8_general_ci")
                        .Annotation("MySql:CharSet", "utf8")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.gid);
                    table.ForeignKey(
                        name: "fk_accountid",
                        column: x => x.accountId,
                        principalTable: "account",
                        principalColumn: "gid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_workid",
                        column: x => x.workId,
                        principalTable: "works",
                        principalColumn: "gid",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8")
                .Annotation("Relational:Collation", "utf8_general_ci");

            migrationBuilder.CreateIndex(
                name: "fk_accountid",
                table: "applyJob",
                column: "accountId");

            migrationBuilder.CreateIndex(
                name: "fk_workid",
                table: "applyJob",
                column: "workId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "applyJob");

            migrationBuilder.DropTable(
                name: "account");

            migrationBuilder.DropTable(
                name: "works");
        }
    }
}
