﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
  public static class DataServiceExtension
  {
    /// <summary>
    /// 注入数据
    /// </summary>
    /// <param name="services"></param>
    public static IServiceCollection AddDataService(this IServiceCollection services)
    {
      #region 依赖注入
      //services.AddScoped<IUserService, UserService>();
      var baseType = typeof(DataBase.IAutoAddDenpendency);
      var path = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
      var referencedAssemblies = System.IO.Directory.GetFiles(path, "*.dll").Select(Assembly.LoadFrom).ToArray();
      var types = referencedAssemblies
          .SelectMany(a => a.DefinedTypes)
          .Select(type => type.AsType())
          .Where(x => x != baseType && baseType.IsAssignableFrom(x)).ToArray();
      var implementTypes = types.Where(x => x.IsClass).ToArray();
      var interfaceTypes = types.Where(x => x.IsInterface).ToArray();
      foreach (var implementType in implementTypes)
      {
        var interfaceType = interfaceTypes.FirstOrDefault(x => x.IsAssignableFrom(implementType));
        if (interfaceType != null)
          services.AddScoped(interfaceType, implementType);
      }

      #endregion

      return services;
    }
  }
}
