﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class HangfireSetting
    {
        public string ServerName { get; set; }
        public int RedisDb { get; set; }
        public string RedisPrefix { get; set; }
        public string RedisConnStr { get; set; }
    }
}
