using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class SystemConfig
    {
        /// <summary>
        /// 站点名称
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// 开启中铁一体化验证
        /// </summary>
        public string IsCrecAuth { get; set; }


        /// <summary>
        /// 验证服务器地址
        /// </summary>
        public string AuthServer { get; set; }


        /// <summary>
        /// 消息推送服务器
        /// </summary>
        public string PushServer { get; set; }
        

        /// <summary>
        /// 平台ID
        /// </summary>
        public string ClientID { get; set; }

        /// <summary>
        /// 中铁E通 移动端应用应用标识
        /// </summary>
        public string AgentId { get; set; }

        /// <summary>
        /// 中铁E通 移动端应用应用秘钥
        /// </summary>      
        public string AgentSecret { get; set; }


        /// <summary>
        /// 接口调用标识
        /// </summary>
        public string ConsumerCode { get; set; }
        
        /// <summary>
        /// 接口秘钥
        /// </summary>
        public string ApiSecret { get; set; }

        /// <summary>
        /// 平台ID秘钥
        /// </summary>
        public string ClientSecret { get; set; }


        /// <summary>
        /// 验证通过回调URI
        /// </summary>
        public string CrecRedirectUri { get; set; }


     
        /// <summary>
        /// 访问平台接口密钥
        /// </summary>
        [JsonIgnore]
        public string ApiKey { get; set; }
        /// <summary>
        /// 当前部署协议
        /// http或https
        /// 默认http
        /// </summary>
        [JsonIgnore]
        public string Protocol { get; set; }
        /// <summary>
        /// 当前部署域名
        /// </summary>
        [JsonIgnore]
        public string Domain { get; set; }
        /// <summary>
        /// 运行模式
        /// 无限制
        /// 微信（只能在微信浏览器中打开）
        /// </summary>
        public string RunMode { get; set; }

        /// <summary>
        /// 是否允许普通浏览器访问
        /// </summary>
        public bool EnabledBrower { get; set; }

        public bool IsDebugMode { get; set; }

        public string RegCode { get; set; }

        /// <summary>
        /// 百度地图JS Api 密钥
        /// </summary>
        public string BaiduMapAK { get; set; }

        /// <summary>
        /// 腾讯地图JS Api 密钥
        /// </summary>
        public string QQMapKey { get; set; }


        /// <summary>
        /// 高德地图JS Api 密钥
        /// </summary>
        public string AMapKey { get; set; }


        /// <summary>
        /// 第三方自动登录密钥
        /// </summary>
        [JsonIgnore]
        public string AutoLoginKey { get; set; }


        /// <summary>
        /// 企业微信通讯录操作密钥
        /// </summary>
        [JsonIgnore]
        public string AddressBookSecret { get; set; }


        /// <summary>
        /// 自定义CSS
        /// </summary>
        public string CustomCss { get; set; }

        /// <summary>
        /// 报告链接是否加密
        /// </summary>
        public string ReportEncrypt { get; set; }

        /// <summary>
        /// 报告真伪验证方式
        /// local：本地
        /// remote：http://wx.zgjyjc.cn/Q/Q.aspx
        /// </summary>
        public string ReportCheckMode { get; set; }

        /// <summary>
        /// 登录页面是否显示LOGO
        /// </summary>
        public bool ShowLogo { get; set; } = true;


        /// <summary>
        /// 综合查询界面显示结论的项目
        /// </summary>
        public string SearchShowConclusionCodes { get; set; }

        /// <summary>
        /// 委托详情显示总结论的项目
        /// </summary>
        public string EntrustDetailShowConclusionTextCodes { get; set; }

        /// <summary>
        /// 菜单版本
        /// </summary>
        public string MenuVersion { get; set; }

        /// <summary>
        /// 是否启用监管单位登录 
        /// </summary>
        public bool EnabledSupervision { get; set; }

        public string Url
        {
            get
            {
                return string.Format("{0}://{1}", Protocol, Domain);
            }
        }

        public Dictionary<string, object> CustomConfig { get; set; }
        public Dictionary<string, string> ApiHostList { get; set; }
        /// <summary>
        /// 是否为微信运行模式
        /// </summary>
        //public bool WeiXinMode
        //{
        //    get
        //    {
        //        return RunMode != null && RunMode.Equals("WeiXin", StringComparison.CurrentCultureIgnoreCase);
        //    }
        //}

        /// <summary>
        /// 根据配置文件，判断当前运行模式：公众号，还是企业微信
        /// </summary>
        public RunMode ConfigRunMode
        {
            get
            {
                if (string.IsNullOrEmpty(RunMode))
                {
                    return Models.RunMode.Auto;
                }

                if (RunMode.Equals("WeiXin", StringComparison.CurrentCultureIgnoreCase))
                {
                    return Models.RunMode.MP;
                }
                if (RunMode.Equals("WeiXinWork", StringComparison.CurrentCultureIgnoreCase)
                    || RunMode.Equals("Work", StringComparison.CurrentCultureIgnoreCase))
                {
                    return Models.RunMode.Work;
                }
                if (RunMode.Equals("DingTalk", StringComparison.CurrentCultureIgnoreCase))
                {
                    return Models.RunMode.DingTalk;
                }
                return Models.RunMode.Brower;
            }
        }

        //public List<string> Check()
        //{
        //    List<string> errMsg = new List<string>();
        //    if (string.IsNullOrEmpty(ApiHost))
        //    {
        //        errMsg.Add(string.Format("平台接口地址未配置：{0}", "ApiHost"));
        //    }
        //    if (string.IsNullOrEmpty(Domain))
        //    {
        //        errMsg.Add(string.Format("微信部署域名未配置：{0}", "Domain"));
        //    }
        //    return errMsg;
        //}
    }

    public enum RunMode
    {
        /// <summary>
        /// 自动判断
        /// </summary>
        Auto = 0,
        /// <summary>
        /// 普通浏览器
        /// </summary>
        Brower = 1,
        /// <summary>
        /// 微信公众号
        /// </summary>
        MP = 2,
        /// <summary>
        /// 企业微信
        /// </summary>
        Work = 3,
        /// <summary>
        /// 钉钉
        /// </summary>
        DingTalk = 4,
        /// <summary>
        /// AppWorker
        /// </summary>
        AppWorker = 5
    }
}
