﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DataBase
{
  public class DataPageResult<T>
  {
    public int Count { get; set; }
    public List<T> List { get; set; }
  }
}
