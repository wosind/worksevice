﻿using Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainService
{
  public interface IAccountService : IAutoAddDenpendency
  {
    Account GetAccountByWid(string wid);
    Account AddItem(Account _account);

  }
}
