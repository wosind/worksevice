﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Models.DataBase;

namespace MainService
{
  public class OpenIdOfWxOpen
  {
    public string session_key { get; set; }
    public string openid { get; set; }
    public string unionid { get; set; }
  }

  public class ParmGenToken
  {
    public string Name { get; set; }
    public List<string> Roles { get; set; }
    public string Wid { get; set; }
    public string Secret { get; set; }
    public string Phone { get; set; }
  }

  public interface IAuthService : IAutoAddDenpendency
  {
    OpenIdOfWxOpen GetOpenId(string codes, string encryptedDataStr, string iv);
    string GetPhoneNum(string code);
    BaseReturn<Account> GenToken(string wid);
    BaseReturn<Account> CreateId(ParmGenToken model);
  }
}
