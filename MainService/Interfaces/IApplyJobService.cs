﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.DataBase;
using Common;
namespace MainService
{
  public interface IApplyJobService : IAutoAddDenpendency
  {
    BaseReturn<string> AddItem(ApplyJob applyJob);
    List<ApplyJob> GetApplyJobsByAccountId(string accountId);
  }
}
