﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.DataBase;
using DataRepository;
namespace MainService
{
  public interface IWorkService : IAutoAddDenpendency
  {
    DataPageResult<Work> GetWorkList(int page, int limit);
    Work AddItem(Work work);
    Work GetWorkById(string gid);
  }
}
