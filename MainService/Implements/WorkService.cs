﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataRepository;
using Models.DataBase;

namespace MainService
{
  public class WorkService : IWorkService
  {
    public readonly IWorkRepository _workRepository;
    public WorkService(IWorkRepository workRepository)
    {
      _workRepository = workRepository;
    }   

    public Work AddItem(Work work)
    {
      if (work == null) return null;
      if (_workRepository.IsExists(work)) return null;
      return _workRepository.AddItem(work);
    }

    public Work GetWorkById(string gid)
    {
      if (gid == null) return null;
      return _workRepository.GetDetailById(gid);
    }

    public DataPageResult<Work> GetWorkList(int page, int limit)
    {
      return _workRepository.GetWorkList(page, limit);
    }
    
  }
}
