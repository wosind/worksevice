﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataRepository;
using Models.DataBase;

namespace MainService
{
  public class AccountService : IAccountService
  {
    public readonly IAccountRepository _AccountRepository;
    public AccountService(IAccountRepository AccountRepository)
    {
      _AccountRepository = AccountRepository;
    }

    public Account AddItem(Account _account)
    {
      return _AccountRepository.AddItem(_account);
    }

    public Account GetAccountByWid(string wid)
    {
      return _AccountRepository.GetAccountByWid(wid);
    }
  }
}
