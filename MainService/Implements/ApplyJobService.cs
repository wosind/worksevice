﻿using Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataRepository;
using Common;
using Newtonsoft.Json;

namespace MainService
{
  public class ApplyJobService : IApplyJobService
  {
    public readonly IApplyJobRepository _applyJobRepository;
    public ApplyJobService(IApplyJobRepository applyJobRepository)
    {
      _applyJobRepository = applyJobRepository;
    }

    public BaseReturn<string> AddItem(ApplyJob applyJob)
    {
      if (_applyJobRepository.IsExists(applyJob))
      {
        return new BaseReturn<string> { Success = false, ErrMsg = "您已经申请了这份工作" };
      }
      else
      {
        _applyJobRepository.AddItem(applyJob);
        return new BaseReturn<string>();
      }

    }

    public List<ApplyJob> GetApplyJobsByAccountId(string accountId)
    {   
      return _applyJobRepository.GetApplyJobs(accountId);
    }
  }
}
