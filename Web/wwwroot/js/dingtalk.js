﻿function qrCodeScan(callback) {
    toast('正在扫描二维码');
    dd.ready(function() {
        dd.biz.util.scan({
            type: 'qrCode' , // type 为 all、qrCode、barCode，默认是all。
            onSuccess: function (data) {
                if (typeof (callback) == "function")
                    callback(data.text)
                else
                    qrCodeParse(data.text);
            },
            onFail : function(err) {
            }
        })
    });
}

function scanReport() {
    toast('正在验证报告');
    dd.ready(function () {
        dd.biz.util.scan({
            type: 'qrCode', // type 为 all、qrCode、barCode，默认是all。
            onSuccess: function (data) {
                scanReportParse(data.text);
            },
            onFail: function (err) {
            }
        })
    });
};

function dingtalkAlert() {
    dd.device.notification.alert({
        message: "测试",
        title: "提示",//可传空
        buttonName: "收到",
        onSuccess : function() {
            //onSuccess将在点击button之后回调
            /*回调*/
        },
        onFail : function(err) {}
    });
}

