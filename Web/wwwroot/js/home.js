﻿
//选择站点
function selectStation() {
    mui('#stationList').popover('toggle');
}

//切换站点
function changeStation(stationCode) {
    axios.get("/User/ChangeStation", { params: { stationCode: stationCode } }).then(function (r) {
        var data = r.data;
        if (data.code == 0) {
            showAlert2("已切换到：" + data.data, function () {
                window.location.reload();
            });
            
        } else {
            showAlert("切换失败，原因：" + data.msg);
        }
    });
}

//选择站点
function selectApiHost() {
    mui('#apiHostList').popover('toggle');
}

//切换站点
function changeApiHost(apiHost) {
    //为什么用post，因为如果使用get请求，参数中包含localhost会有问题，不会调用后台接口，直接返回(仅限微信端)
    var data = { apiHost: apiHost };
    axios.post("/User/ChangeApiHost", data).then(function (r) { 
        console.dir(r);
        var data = r.data;
        if (data.code == 0) {
            showAlert2("已切换成功", function () {
                window.location.reload();
            });
        } else {
            showAlert("切换失败，原因：" + data.msg);
        }
    }).catch((e) => {
        console.log(e)
    });
}



var testIndex = 1;
var testInterval;
Zepto(function ($) {
    mui.ready(function () {
        //showLoading();
        //testInterval = setInterval(function () {
        //    setLoadingText(testIndex);
        //    testIndex++;
        //    if (testIndex > 10) {
        //        clearInterval(testInterval)
        //        closeLoading();
        //    }
        //},1000);
    });
})
