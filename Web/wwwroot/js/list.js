﻿var pageIndex = 1;
var totalPageCount = 0;

function pulldownRefresh() {
    setTimeout(function () {
        pageIndex = 1;
        getData(true, function () {
            mui('#pullrefresh').pullRefresh().endPulldownToRefresh(pageIndex > totalPageCount);
            if (pageIndex < totalPageCount) {
                mui('#pullrefresh').pullRefresh().refresh(true);
            }
        });
    }, 1500);
}

function pullupRefresh() {
    setTimeout(function () {
        getData(false, function () {
            mui('#pullrefresh').pullRefresh().endPullupToRefresh(pageIndex > totalPageCount);
        });
    }, 1500);
}

function getTpl(url, cb) {
    axios.get(url).then(cb);
}

function reloadData() {
    closeModel();
    getData(true, function () {

    });
}

var dataResult = null;

function getData(isRefresh, cb) {
    if (isRefresh) {
        pageIndex = 1;


    }
    if (pageIndex == 1) {
        dataResult = null;
    }
    toast('正在加载数据...');
    var keyword = $('searchInput').length > 0 ? $('searchInput').val() : '';
    if (!queryParms) queryParms = {};
    queryParms.k = keyword;
    queryParms.p = pageIndex;
    if (typeof (currentAdvWhere) != undefined) {
        for (var p in currentAdvWhere) {
            queryParms[p] = currentAdvWhere[p];
        }
    }
    if (typeof (getDefaultWhere) == "function") {
        var where = getDefaultWhere();
        for (var p in where) {
            queryParms[p] = where[p];
        }
    }
    
    if (typeof (tplId) == "undefined" || tplId.length == 0) {
        tplId = "listTpl";
    }

    axios.get(dataUrl, {
        params: queryParms
    }).then(function (r) {
        var data = r.data;
        if (data.code == 0) {
            toast('获取数据成功');
            totalPageCount = data.pageCount;
            if (isRefresh || pageIndex % 10 == 0) { //如果是刷新，或每10页清一次页面缓存
                document.getElementById('list').innerHTML = "";
                dataResult = null;
            }
            if (dataResult == null) {
                dataResult = [];
            }
            for (var i = 0; i < data.data.length; i++) {
                dataResult.push(data.data[i]);
            }
            var html = template(tplId, data);
            document.getElementById('list').innerHTML += html;
            pageIndex++;
            if (typeof (cb) == "function") {
                cb();
            }
            if (typeof (loadSuccess) == "function") {
                loadSuccess();
            }
        } else {
            var result = parseErrorHtml(data, "获取数据失败");
            showAlert(result);
        }
    });
}

function search() {
    pageIndex = 1;
    getData(true);
}
readyFun(function () {
    mui.init({
        pullRefresh: {
            container: '#pullrefresh',
            down: {
                callback: pulldownRefresh
            },
            up: {
                callback: pullupRefresh
            }
        }
    });
    mui.ready(function () {
        document.getElementById('searchInput').onchange = _.debounce(search, 500);
        mui('#pullrefresh').pullRefresh().pullupLoading();
    });
    template.defaults.imports.abs = function (value) { return Math.abs(value) };
});