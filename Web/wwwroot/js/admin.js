﻿function syncAllConfig() {
    showLoading("正在处理...");
    axios.get("/API/Config/SyncAll", { params: {} }).then(function (r) {
        closeLoading();
        var data = r.data;
        if (data.code == 0) {
            showAlert2("同步所有配置完成");

        } else {
            showAlert("同步失败，原因：" + data.msg);
        }
    }).catch((error) => {
        closeLoading();
        if (error.response) {
            showAlert2("返回结果错误：" + error.response.status);
        } else if (error.request) {
            showAlert2("请求错误：" + error.message);
        } else {
            showAlert2(error.message);
        }
        console.log(error);
    });
}

function showAllConfig() {
    showLoading("正在处理...");
    axios.get("/API/Config/GetAllConfig", { params: {} }).then(function (r) {
        closeLoading();
        var data = r.data;
        var headerHtml = '<header class="mui-bar mui-bar-nav"><a  onclick="closeModel()" class="mui-icon mui-icon-left-nav mui-pull-left"></a></header>' ;
        if (data.code == 0) {
            showModal(headerHtml + "<div class='mui-content details'><pre style='width:100%;height:100%;'>" + JSON.stringify(data.data, null, 4) + "</pre></div>");
        } else {
            showAlert("同步失败，原因：" + data.msg);
        }
    }).catch((error) => {
        closeLoading();
        if (error.response) {
            showAlert2("返回结果错误：" + error.response.status);
        } else if (error.request) {
            showAlert2("请求错误：" + error.message);
        } else {
            showAlert2(error.message);
        }
        console.log(error);
    });
}

function setup() {
    var btnArray = ['是', '否'];
    confirm('是否开始初始化移动端？', btnArray, function (e) {
        showLoading("正在处理...");
        axios.get("/API/Config/Setup", { params: {} }).then(function (r) {
            var data = r.data;
            if (data.code == 0) {
                showAlert2("初始化完成");
            } else {
                showAlert("初始化失败，原因：" + data.msg);
            }
        }).catch((error) => {
            closeLoading();
            if (error.response) {
                showAlert2("返回结果错误：" + error.response.status);
            } else if (error.request) {
                showAlert2("请求错误：" + error.message);
            } else {
                showAlert2(error.message);
            }
            console.log(error);
        });
    })
}

function resetCache() {
    var btnArray = ['是', '否'];
    confirm('是否重置缓存？', btnArray, function (e) {
        showLoading("正在处理...");
        axios.get("/API/Config/ResetCache", { params: {} }).then(function (r) {
            closeLoading();
            var data = r.data;
            if (data.code == 0) {
                showAlert2("重置完成");
            } else {
                showAlert("重置失败，原因：" + data.msg);
            }
        }).catch((error) => {
            closeLoading();
            if (error.response) {
                showAlert2("返回结果错误：" + error.response.status);
            } else if (error.request) {
                showAlert2("请求错误：" + error.message);
            } else {
                showAlert2(error.message);
            }
            console.log(error);
        });
    })
}