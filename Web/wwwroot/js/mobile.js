function qrCodeParse(resultStr) {
    toast('正在解析二维码内容');
    axios.post("/QrCode/Parse", {
        qrcode: resultStr
    }).then(function (r) {
        var data = r.data;
        if (data.code == 0) {
            var result = data.data;
            console.dir(result);
            if (result == null) {
                showAlert("不支持此二维码格式");
                return;
            }
            if (result.hasError) {
                showAlert("错误：" + result.msg);
            } else {
                if (result.mode == "redirect") {
                    if (result.url != null) {
                        mui.openWindow({
                            id: 'qrCodeResult',
                            url: result.url
                        })
                    }
                } else if (result.mode == "ajax") {
                    if (result.url != null) {
                        axios.get(result.url).then(function (r) {
                            var data = r.data;
                            var result = data.data;
                            console.dir(data);
                            if (data.code == 0) {
                                showAlert(data.msg);
                            } else {
                                showAlert("错误：" + data.msg);
                            }
                        });
                    }
                } else {
                    showAlert("不支持此二维码");
                }
            }

        } else {
            showAlert("解析失败，原因：" + data.msg);
        }
    });
}

function scanReportParse(resultStr) {
    if (systemConfig.ReportCheckMode == "remote") {
        mui.openWindow({
            id: 'reportCheck',
            url: "http://wx.zgjyjc.cn/Q/Q.aspx?s=" + resultStr
        });
        return;
    }
    axios.get("/Report/Check", {
        params: {
            code: resultStr
        }
    }).then(function (r) {
        var data = r.data;
        if (data.code == 0) {
            var btnArray = ['是', '否'];
            confirm(data.msg + "\r\n是否查看报告详情？", btnArray, function (e) {
                mui.openWindow({
                    id: 'reportCheck',
                    url: data.data
                })
            });
        } else {
            showAlert("验证失败，原因：" + data.msg);
        }
    });
}