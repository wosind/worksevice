﻿function chooseImage(success, parmInfo, sourceType) {
    if (typeof (sourceType) == "undefined") {
        sourceType = ['album', 'camera'];
    }
    wx.chooseImage({
        count: 9, // 默认9
        sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: sourceType, // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
            console.dir(res);
            getChooseImageList(res, success, parmInfo);
        }
    });
}

function uploadImage(localId) {
    wx.uploadImage({
        localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
        isShowProgressTips: 1, // 默认为1，显示进度提示
        success: function (res) {
            var serverId = res.serverId; // 返回图片的服务器端ID
            console.dir(res);
        }
    });
}

function downloadImage(serverId) {
    wx.downloadImage({
        serverId: serverId, // 需要下载的图片的服务器端ID，由uploadImage接口获得
        isShowProgressTips: 1, // 默认为1，显示进度提示
        success: function (res) {
            var localId = res.localId; // 返回图片下载后的本地ID
            console.dir(res);
        }
    });
}

function getLocalImage(localIds) {
    var localId = localIds[0];
    wx.getLocalImgData({
        localId: localId, // 图片的localID
        success: function (res) {
            var localData = res.localData; // localData是图片的base64数据，可以用img标签显示
            console.dir(localData);
        }
    });
}

function getChooseImageList(res, success, parmInfo) {
    if (true) {
        console.info("is_wkwebview");
        for (var i = 0; i < res.localIds.length; i++) {
            console.info("localId:" + res.localIds);
            wx.getLocalImgData({
                localId: res.localIds[i],
                success: function (res2) {
                    console.dir(res2);
                    success(res2.localData, parmInfo);
                },
                fail: function (res2) {
                    console.dir(res2);
                }
            });
        }
    } else {
        console.info("is_wkwebview2");
        $.each(res.localIds,
            function (index, el) {
                success(el, parmInfo);
            });
    }
}

function getLocation(success) {
    wx.getLocation({
        type: 'gcj02', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
        success: success
    });
    //var latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
    //var longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
    //var speed = res.speed; // 速度，以米/每秒计
    //var accuracy = res.accuracy; // 位置精度
}

function openLocation(res) {
    wx.openLocation({
        latitude: res.latitude, // 纬度，浮点数，范围为90 ~ -90
        longitude: res.longitude, // 经度，浮点数，范围为180 ~ -180。
        name: res.name, // 位置名
        address: res.address, // 地址详情说明
        scale: 18, // 地图缩放级别,整形值,范围从1~28。默认为最大
        infoUrl: '' // 在查看位置界面底部显示的超链接,可点击跳转
    });
}

//通用扫码
function qrCodeScan(callback) {
    toast('正在扫描二维码');
    wx.scanQRCode({
        needResult: 1,
        desc: 'scanQRCode desc',
        success: function (res) {
            var resultStr = res.resultStr;
            if (typeof (callback) == "function")
                callback(resultStr)
            else
                qrCodeParse(resultStr);
        }
    });
}

function scanReport() {
    toast('正在验证报告');
    wx.scanQRCode({
        needResult: 1,
        desc: 'scanQRCode desc',
        success: function (res) {
            var resultStr = res.resultStr;
            scanReportParse(resultStr);
        }
    });
};