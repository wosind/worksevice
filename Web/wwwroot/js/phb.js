﻿

function setPhbData(item) {
    $('#jzphb').val(item['num']);
    var mapping = {
        "jz_water": "jz_water",
        "gg_sn": ["gg_sn", "qd_sn"],
        "jz_sn": "jz_sn",
        "gg_sha1": "gg_sha1",
        "jz_sha1": "jz_sha1",
        "gg_sha2": "gg_sha2",
        "jz_sha2": "jz_sha2",
        "gg_shi1": ["gg_shi1", "size_shi1"],
        "jz_shi1": "jz_shi1",
        "gg_shi2": ["gg_shi2", "size_shi2"],
        "jz_shi2": "jz_shi2",
        "gg_chl1": "gg_chl1",
        "jz_chl1": "jz_chl1",
        "gg_chl2": "gg_chl2",
        "jz_chl2": "jz_chl2",
        "gg_chl3": "gg_chl3",
        "jz_chl3": "jz_chl3",
        "gg_wjj1": "gg_wjj1",
        "jz_wjj1": "jz_wjj1",
        "gg_wjj2": "gg_wjj2",
        "jz_wjj2": "jz_wjj2",
        "jz_qt": "jz_qt",
    };
    for (var attr in mapping) {
        var field = mapping[attr];
        var v = "";
        if (typeof (field) == "string") {
            v = item[field];
        } else {
            v = "";
            for (var i = 0; i < field.length; i++) {
                v += item[field[i]];
            }
        }

        $('#' + attr).val(v);
        var xdField = attr.replace('jz_', 'xd_');
        var oValue = $('#' + xdField).val();
        if (oValue == null || oValue.length == 0) {
            $('#' + xdField).val(v);
        }
    }
    calcTotal();
}

function calcTotal() {
    var scfl = parseFloat($('#scfl').val());
    var fieldList = ['sn', 'sha1', 'sha2', 'shi1', 'shi2', 'water', 'wjj1', 'wjj2', 'chl1', 'chl2', 'chl3'];
    for (var i = 0; i < fieldList.length; i++) {
        calc(scfl, fieldList[i]);
    }
}

function calc(scfl, field) {
    var jz = calcField(scfl, field, 'jz');
    var xd = calcField(scfl, field, 'xd');
    var tz = parseFloat($('#tz_' + field).val());
    var v = isNaN(tz) || tz == 0 ? xd : tz;
    calcPc(field, jz, v);
}
    
function calcField(scfl, field, type) {
    var v = parseFloat($('#' + type + '_' + field).val());
    var total = v * scfl;
    if (isNaN(total))
        total = null;
    $('#' + type + '_' + field + "_total").val(total);
    return v;
}

//偏差预警值配置
var warningConfig = {
    "sn": 5, //水泥
    "sha1": 5,  //砂
    "sha2": 5,  //砂
    "shi1": 5, //石
    "shi2": 5, //石
    "wjj1": 10, //外加剂
    "wjj2": 10, //外加剂
    "chl1": 5, //掺合料1-粉煤灰
    "chl2": 5  //掺合料2-矿粉
}

function checkPc(field) {
    restoreColor(field);
    var warnValue = warningConfig[field];
    if (typeof (warnValue) == "undefined") return false;
    if (field == "sn" || field == "chl1" || field == "chl2") {
        var snPc = parseFloat($('#pc_sn').val());
        var chl1Pc = parseFloat($('#pc_chl1').val());
        var chl2Pc = parseFloat($('#pc_chl2').val());
        var totalPc = Math.abs(snPc) + Math.abs(chl1Pc) + Math.abs(chl2Pc);
        if (totalPc >= 10) { //胶凝材料（水泥+矿粉+粉煤灰）
            changeColor("sn");
            changeColor("chl1");
            changeColor("chl2");
            return;
        } else {
            restoreColor("sn");
            restoreColor("chl1");
            restoreColor("chl2");
        }
    }
    var v = parseFloat($('#pc_' + field).val());
    var b = Math.abs(v) >= warnValue;
    if (b)
        changeColor(field);
}


function calcPc(field, jzTotal, xdTotal) {
    if (xdTotal == null || xdTotal == null) {
        $('#pc_' + field).val('');
        return;
    }
    var v = Math.round((xdTotal - jzTotal) / jzTotal * 100);
    if (!isNaN(v) && - isFinite(v))
        $('#pc_' + field).val(v);
    else
        $('#pc_' + field).val('');
    checkPc(field);

}

function calcXd() {
    var field = $(this).attr('id').replace('xd_', '');
    var scfl = parseFloat($('#scfl').val());
    calc(scfl, field);
}

function initPhb() {
    $('#scfl').on('change', function () {
        calcTotal();
    });
    $("input[id^='xd_']").on('change', calcXd).on('keyup', calcXd);
    calcTotal();
}

$(function () {
    initPhb();
});


function changeColor(f) {
    var oldColor = $('#pc_' + f).css("color");
    if (!$('#pc_' + f).attr("data-border-color"))
        $('#pc_' + f).attr("data-border-color", oldColor);
    $('#pc_' + f).css("color", "red");
}

function restoreColor(f) {
    var oldColor = $('#pc_' + f).attr("data-border-color");
    if ($('#pc_' + f).attr("data-border-color"))
        $('#pc_' + f).css("color", oldColor);
}