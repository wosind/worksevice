﻿//AppWorker 返回兼容 
window.onDeviceOneLoaded = function () {
    var nf = sm("do_Notification");
    var app = sm("do_App");
    var page = sm("do_Page");

    var homeLink = ['/Todo', '/Instrument/List', '/Entrust/Search', '/Report/Unqualified', '/Message/List', '/ToDo/TestTree']
    var pathName = window.location.pathname;
    if (homeLink.indexOf(pathName) != -1) {
        var backEl = mui('.mui-action-back');
        if (backEl.length > 0) {
            backEl[0].onclick = function () {
                app.closePage();
            }
        }
    }
    console.log('AppWorker测试：');
    var bdloaction = sm("do_BaiduLocation");
    var do_Algorithm = sm("do_Algorithm");
    console.dir(bdloaction);
    bdloaction.locate("middle", function (data, e) {  
        var appWokerGpsInfo = bMapToQQMap(data);

        appWokerGpsInfo.accuracy = 0; 
        localStorage.setItem("appWokerGpsInfo",JSON.stringify(appWokerGpsInfo));
    })
    var camera = sm("do_Camera");
    console.dir(camera);
    if (typeof (customizeFun) == "function") {
        customizeFun();
    }
    //alert("camera");
    //camera.capture(720, -1, 72, false, function (d) {
    //    alert(d);
    //});

    //camera.capture({  quality: 50, iscut: true, facingFront: false}, function (data, e) {
    //    alert(data)
    //    $('#test').attr('src', 'data:image/jpeg;base64,' + data);
    //    var base64String;
    //    do_Algorithm.base64({ type: "encode", sourceType: "file", source: data }, function (data2, e) {
    //        //alert(data2);
    //        base64String = data2;
    //        $('#test').attr('src', 'data:image/jpeg;base64,' + data2);
    //    })
    //})

}
function bMapToQQMap(info) {
    var lng = info.longitude; var lat = info.latitude;
    if (lng == null || lng == '' || lat == null || lat == '')
        return [lng, lat];

    var x_pi = 3.14159265358979324;
    var x = parseFloat(lng) - 0.0065;
    var y = parseFloat(lat) - 0.006;
    var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
    var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
    var lng = (z * Math.cos(theta)).toFixed(7);
    var lat = (z * Math.sin(theta)).toFixed(7);

    return { longitude: lng, latitude: lat };

}

