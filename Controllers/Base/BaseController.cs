﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Common;
using Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using Serilog;

namespace Web.Controllers
{
    /// <summary>
    /// 控制器基类
    /// </summary>
    public class BaseController : Controller
    {    
        protected IMemoryCache MemoryCache { get; set; }
        protected readonly ILogger<BaseController> _logger;
        protected Serilog.ILogger Log
        {
            get
            {
                return Serilog.Log.Logger;
            }
        }

        public BaseController()
        {
        }

        public BaseController(IMemoryCache memoryCache)
        {
            MemoryCache = memoryCache;
        }

        public BaseController(ILogger<BaseController> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext != null)
            {
                var isDebug = Request.Query["isDebug"] == "1";
                if (isDebug)
                {
                }
            }
            
            InitViewBag();
            var controllerActionDescriptor = filterContext.ActionDescriptor as ControllerActionDescriptor;
            if (controllerActionDescriptor != null)
            {
                var isDefined = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                    .Any(a => a.GetType().Equals(typeof(AllowAnonymousAttribute)));
                if (isDefined)
                {
                    return;
                }
            }         
         
        }

        private void InitViewBag()
        {            
            ViewBag.NotBack = false;           
        }     
       
    }
}
