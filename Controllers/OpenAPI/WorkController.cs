﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Models.DataBase;
using MainService;


namespace Web.Controllers
{

  /// <summary>
  /// 游客权限，无需登录
  /// </summary>
  [AllowAnonymous]
  [Route("api/Open/[controller]")]
  public class WorkController : BaseController
  {
#pragma warning disable 1591
    public readonly IWorkService _WorkService;
    public WorkController(IWorkService workService)
    {
      _WorkService = workService;
    }
#pragma warning restore 1591

    /// <summary>
    /// 获取职位列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    public async Task<IActionResult> GetWorkList(int page, int limit)
    {
      var res = _WorkService.GetWorkList(page, limit);
      return await PublicTool.ReturnSuccess(res);
    }

    /// <summary>
    /// 获取职位明细
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("[action]")]
    public async Task<IActionResult> GetDetailById(string id)
    {
      var res = _WorkService.GetWorkById(id);
      return await PublicTool.ReturnSuccess(res);
    }


    /// <summary>
    /// 获取职位列表
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    public async Task<IActionResult> AddWork ([FromBody] Work work )
    {
      var res = _WorkService.AddItem(work);
      return await PublicTool.ReturnSuccess("ok");
    }

  }
}
