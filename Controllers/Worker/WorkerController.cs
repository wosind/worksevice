﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Models;
using MainService;
using Models.DataBase;

namespace Web.Controllers
{
  /// <summary>
  /// Worker权限级Controller
  /// </summary>
  [Authorize(Roles = "worker")]
  [Route("api/[controller]")]
  public class WorkerController : BaseController
  {
#pragma warning disable 1591
    public readonly IApplyJobService _applyJobService;
    public WorkerController(IApplyJobService applyJobService)
    {
      _applyJobService = applyJobService;
    }
#pragma warning restore 1591

    /// <summary>
    /// 申请职位
    /// </summary>
    /// <param name="applyJob"></param>
    /// <returns></returns>
    [HttpPost("[action]")]
    public async Task<IActionResult> SubmitApplyJob([FromBody] ApplyJob applyJob)
    {

      var _res = _applyJobService.AddItem(applyJob);

      if (_res.Success)
      {
        return await PublicTool.ReturnSuccess("职位已申请");
      }
      else
      {
        return await PublicTool.ReturnError(_res.ErrMsg);
      }
    }

    /// <summary>
    /// 通过AccountId检索职位申请列表
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("[action]")]
    public async Task<IActionResult> GetApplyJobsById(string id)
    {    
      var res = _applyJobService.GetApplyJobsByAccountId(id);
      return await PublicTool.ReturnSuccess(res);
    }
  }
}
